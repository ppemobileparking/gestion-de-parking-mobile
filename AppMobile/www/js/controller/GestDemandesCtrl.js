app.controller('GestDemandesCtrl', function ($scope, $resource, $http, $cookies, $state) {
    $scope.getDemandes = function () {
        var result = [];

        var Users = $http.get('http://104.46.44.90:8082/v1/user/actif/');
        Users.success(function (usersData) {
            for(var i in usersData){
                result[usersData[i].ID] =usersData[i].mail;
            }
            $scope.users = result;
        });
        var Demandes = $http.get('http://104.46.44.90:8082/v1/demande/getDemandes/');
        Demandes.success(function (data) {
            $scope.demandes = data;
        });
        Demandes.error(function (data) {
            alert("failure message: " + JSON.stringify({data: data}));
        });
    };
    $scope.getDemandesForUser = function () {
        var usermail = $cookies.get("usermail");
        var user;
        var userRequest = $http.get('http://104.46.44.90:8082/v1/user/getbyemail/' + usermail);
        userRequest.success(function (data) {
           user = data;
            var result = [];
            var Demandes = $http.get('http://104.46.44.90:8082/v1/demande/getDemandes/');
            Demandes.success(function (data) {
                for (var i in data){
                    if(data[i].Userid == user.ID){
                        result.push(data[i]);
                    }
                }
                $scope.demandes = result;
            });
            Demandes.error(function (data) {
                alert("failure message: " + JSON.stringify({data: data}));
            });
        });

    };
    $scope.getDemandeById = function(){
        var demande = $http.get('http://104.46.44.90:8082/v1/demande/get/'+demande.id);
        demande.success(function(data){
            $scope.d = data;
        })
    };

    $scope.newDemande = function(Allee, Place){
        var usermail = $cookies.get("usermail");
        var user;
        var userRequest = $http.get('http://104.46.44.90:8082/v1/user/getbyemail/' + usermail);
        userRequest.success(function (data) {
            var demande = {
                num_allee: Allee,
                num_place: Place,
                handicape: data.handicap,
                userId: data.ID
            };
            $http.defaults.headers.post["Content-Type"] = "application/json";
            var placeRequest = $http.post('http://104.46.44.90:8082/v1/demande/new/', demande);
            placeRequest.success(function () {
                $scope.getDemandesForUser();
                $scope.result = "Demande Envoyée";
            });
        });
    };
});