app.controller('LoginCtrl', function ($scope, $resource, $state, $stateParams, $http, $cookies) {
    $scope.loginUser = function (mail) {
        var user = $http.get('http://104.46.44.90:8082/v1/user/getbyemail/' + mail);
        user.success(function (data) {
            $scope.message = data;
            if (data.mail == mail) {
                $cookies.put("usermail", mail);
                if(data.ID == 4 || data.ID == 10){
                    $state.go('admin', {user: user});
                }else{
                    $state.go('suivisDemandesUser', {user: user});
                }
            }else{
                $scope.error = "L'adresse "+mail+" est inconnue... Veuillez vous inscrire !";
            }
        });
        user.error(function (data) {
            alert("failure message: " + JSON.stringify({data: data}));
        });
    }
});