app.controller('PlacesCtrl', function ($scope, $http) {
    $scope.insertPlace = function (allee, place, handicap) {
        var newPlace = {
            num_allee: allee,
            num_place: place,
            handicape: handicap
        };
        $http.defaults.headers.post["Content-Type"] = "application/json";
        var placeRequest = $http.post('http://104.46.44.90:8082/v1/place/new/', newPlace);
        placeRequest.success(function (data) {
            $scope.result = "Place ajoutée (id : " + data.ID + ")";
        });
    };
    $scope.getPlaces = function () {
        var result = [];

        var Users = $http.get('http://104.46.44.90:8082/v1/user/actif/');
        Users.success(function (usersData) {
            for(var i in usersData){
                result[usersData[i].ID] =usersData[i].mail;
            }
            $scope.users = result;
        });
        var Places = $http.get('http://104.46.44.90:8082/v1/demande/getPlaces/');
        Places.success(function (data) {
            $scope.places = data;
        });
        Places.error(function (data) {
            alert("failure message: " + JSON.stringify({data: data}));
        });
    };
});