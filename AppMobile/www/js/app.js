function onDeviceReady() {

}

function onBackKeyDown() {
    document.addEventListener("backbutton", function (e) {
        if ($.mobile.activePage.is('#login')) {
            e.preventDefault();
            navigator.app.exitApp();
        } else {
            navigator.app.backHistory()
        }
    }, false);
}

var app = angular.module('app', ['ui.router', 'ngResource', 'ngCookies']);

//Gestion des routes
app.config(function ($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('login', {
            url: '/',
            templateUrl: 'views/login/login.html'
        })
        .state('admin', {
            url: '/admin',
            templateUrl: "views/admin/homeAdmin.html"
        })
        .state('profilOneUser', {
            url: '/profilOneUser/:id',
            templateUrl: 'views/admin/profilUser.html',
            controller: ('ProfilUserCtrl', ['$scope', '$stateParams'],
                function ($scope, $resource, $stateParams) {
                    var User = $resource('http://104.46.44.90:8082/v1/user/get/:id', {
                            id: $stateParams.id
                        },
                        {},
                        {
                            'get': {method: 'GET'}
                        });
                    $scope.user = User.get({}, function (res) {
                        $scope.infoUser = res;
                    })
                })
        })
        .state('addPlace', {
            url:'/addPlace',
            templateUrl: "views/admin/addPlace.html"
        })
        .state('gestUser', {
            url: '/users',
            templateUrl: "views/admin/users.html"
        })
        .state('gestReg', {
            url: '/gestReg',
            templateUrl: "views/admin/gestReg.html"
        })
        .state('gestDemandes', {
            url: '/gestDemandes',
            templateUrl: "views/admin/gestDemandes.html"
        })
        .state('suivisDemandesUser', {
            url: '/suivisDemandes',
            templateUrl: "views/user/suivisDemandes.html"
        })
});
